$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gCoursesDB = {
        description: "This DB includes all courses in system",
        courses: [
            {
                id: 1,
                courseCode: "FE_WEB_ANGULAR_101",
                courseName: "How to easily create a website with Angular",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-angular.jpg",
                teacherName: "Morris Mccoy",
                teacherPhoto: "images/teacher/morris_mccoy.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 2,
                courseCode: "BE_WEB_PYTHON_301",
                courseName: "The Python Course: build web application",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-python.jpg",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 5,
                courseCode: "FE_WEB_GRAPHQL_104",
                courseName: "GraphQL: introduction to graphQL for beginners",
                price: 850,
                discountPrice: 650,
                duration: "2h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-graphql.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 6,
                courseCode: "FE_WEB_JS_210",
                courseName: "Getting Started with JavaScript",
                price: 550,
                discountPrice: 300,
                duration: "3h 34m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 8,
                courseCode: "FE_WEB_CSS_111",
                courseName: "CSS: ultimate CSS course from beginner to advanced",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 14,
                courseCode: "FE_WEB_WORDPRESS_111",
                courseName: "Complete Wordpress themes & plugins",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-wordpress.jpg",
                teacherName: "Clevaio Simon",
                teacherPhoto: "images/teacher/clevaio_simon.jpg",
                isPopular: true,
                isTrending: false
            }
        ]
    }
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
    // Hàm xử lý load trang
    function onPageLoading(){
        "use strict";
        var vArrCoursesPopularResult = getCoursesPopular();
        console.log(vArrCoursesPopularResult);
        displayCoursesPopularToWeb(vArrCoursesPopularResult);

        var vArrCoursesTrendingResult = getCoursesTrending();
        console.log(vArrCoursesTrendingResult);
        displayCoursesTrendingToWeb(vArrCoursesTrendingResult);
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm xử lý lấy ra các course Popular
    function getCoursesPopular(){
        "use strict";
        var vArrayCoursesPopular = [];
        for (var bI = 0; bI < gCoursesDB.courses.length; bI ++){
            if (gCoursesDB.courses[bI].isPopular == true){
                vArrayCoursesPopular.push(gCoursesDB.courses[bI]);
            }
        }
        return vArrayCoursesPopular;
    }
    // Hàm xử lý lấy ra các course Trending
    function getCoursesTrending(){
        "use strict";
        var vArrayCoursesTrending = [];
        for (var bI = 0; bI < gCoursesDB.courses.length; bI ++){
            if (gCoursesDB.courses[bI].isTrending == true){
                vArrayCoursesTrending.push(gCoursesDB.courses[bI]);
            }
        }
        return vArrayCoursesTrending;
    }
    // Hàm hiển thị các courses popular vào div Most Popular
    function displayCoursesPopularToWeb(paramCoursesPopular){
        "use strict";
        var vDivCoursesPopular = $('#div-popular');
        for(var bI = 0; bI < paramCoursesPopular.length; bI++){
            //chia grid system
            var vDivColSm3 = $('<div>')
            .addClass('col-sm-3')
            .appendTo(vDivCoursesPopular);
            //tạo div class card
            var vDivCard = $('<div>')
            .addClass('card')
            .appendTo(vDivColSm3);
            //card-header
            var vImgTopCard = $('<img>', {
                src: paramCoursesPopular[bI].coverImage
            }).addClass('card-img-top mx-auto d-block')
            .appendTo(vDivCard);
            //card body
            var vCardBody = $('<div>').addClass('card-body').appendTo(vDivCard);

            var vCourseNameLink = $('<a>',{
                href: '#',
            }).append($('<h6>', {
                text: paramCoursesPopular[bI].courseName
                }).addClass('card-title'))
            .appendTo(vCardBody);

            var vDuration = $('<i>',{
                text: " " + paramCoursesPopular[bI].duration + " " + '\xa0' + '\xa0' + paramCoursesPopular[bI].level
            }).addClass('far fa-clock')
            .appendTo(vCardBody);

            var vPrice = $('<p>',{
                text: "$" + paramCoursesPopular[bI].discountPrice + " "
            }).addClass('card-text pt-3').append($('<span>',{
                text: "$" + paramCoursesPopular[bI].price
                }).css('text-decoration','line-through'))
            .appendTo(vCardBody);
            //card footer
            var vCardFooter = $('<div>').addClass('card-footer').appendTo(vDivCard);

            var vImgTeacher = $('<img>',{
                src: paramCoursesPopular[bI].teacherPhoto
            }).addClass('rounded-circle')
            .css('width','50')
            .appendTo(vCardFooter);

            var vTeacherName = $('<span>',{
                text: paramCoursesPopular[bI].teacherName
            }).addClass('pl-2')
            .append($('<i>').addClass('far fa-bookmark float-right pt-3 text-secondary'))
            .appendTo(vCardFooter);
        }
    }

    // Hàm hiển thị các courses Trending vào div Trending
    function displayCoursesTrendingToWeb(paramCoursesTrending){
        "use strict";
        var vDivCoursesTrending = $('#div-trending');
        for(var bI = 0; bI < paramCoursesTrending.length; bI++){
            //chia grid system
            var vDivColSm3 = $('<div>')
            .addClass('col-sm-3')
            .appendTo(vDivCoursesTrending);
            //tạo div class card
            var vDivCard = $('<div>')
            .addClass('card')
            .appendTo(vDivColSm3);
            //card-header
            var vImgTopCard = $('<img>', {
                src: paramCoursesTrending[bI].coverImage
            }).addClass('card-img-top mx-auto d-block')
            .appendTo(vDivCard);
            //card body
            var vCardBody = $('<div>').addClass('card-body').appendTo(vDivCard);

            var vCourseNameLink = $('<a>',{
                href: '#',
            }).append($('<h6>', {
                text: paramCoursesTrending[bI].courseName
                }).addClass('card-title'))
            .appendTo(vCardBody);

            var vDuration = $('<i>',{
                text: " " + paramCoursesTrending[bI].duration + " " + '\xa0' + '\xa0' + paramCoursesTrending[bI].level
            }).addClass('far fa-clock')
            .appendTo(vCardBody);

            var vPrice = $('<p>',{
                text: "$" + paramCoursesTrending[bI].discountPrice + " "
            }).addClass('card-text pt-3').append($('<span>',{
                text: "$" + paramCoursesTrending[bI].price
                }).css('text-decoration','line-through'))
            .appendTo(vCardBody);
            //card footer
            var vCardFooter = $('<div>').addClass('card-footer').appendTo(vDivCard);

            var vImgTeacher = $('<img>',{
                src: paramCoursesTrending[bI].teacherPhoto
            }).addClass('rounded-circle')
            .css('width','50')
            .appendTo(vCardFooter);

            var vTeacherName = $('<span>',{
                text: paramCoursesTrending[bI].teacherName
            }).addClass('pl-2')
            .append($('<i>').addClass('far fa-bookmark float-right pt-3 text-secondary'))
            .appendTo(vCardFooter);
        }
    }
           
})