$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gCoursesDB = {
        description: "This DB includes all courses in system",
        courses: [
            {
                id: 1,
                courseCode: "FE_WEB_ANGULAR_101",
                courseName: "How to easily create a website with Angular",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-angular.jpg",
                teacherName: "Morris Mccoy",
                teacherPhoto: "images/teacher/morris_mccoy.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 2,
                courseCode: "BE_WEB_PYTHON_301",
                courseName: "The Python Course: build web application",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-python.jpg",
                teacherName: "Claire Robertson",
                teacherPhoto: "images/teacher/claire_robertson.jpg",
                isPopular: false,
                isTrending: true
            },
            {
                id: 5,
                courseCode: "FE_WEB_GRAPHQL_104",
                courseName: "GraphQL: introduction to graphQL for beginners",
                price: 850,
                discountPrice: 650,
                duration: "2h 15m",
                level: "Intermediate",
                coverImage: "images/courses/course-graphql.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: false
            },
            {
                id: 6,
                courseCode: "FE_WEB_JS_210",
                courseName: "Getting Started with JavaScript",
                price: 550,
                discountPrice: 300,
                duration: "3h 34m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Ted Hawkins",
                teacherPhoto: "images/teacher/ted_hawkins.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 8,
                courseCode: "FE_WEB_CSS_111",
                courseName: "CSS: ultimate CSS course from beginner to advanced",
                price: 750,
                discountPrice: 600,
                duration: "3h 56m",
                level: "Beginner",
                coverImage: "images/courses/course-javascript.jpg",
                teacherName: "Juanita Bell",
                teacherPhoto: "images/teacher/juanita_bell.jpg",
                isPopular: true,
                isTrending: true
            },
            {
                id: 14,
                courseCode: "FE_WEB_WORDPRESS_111",
                courseName: "Complete Wordpress themes & plugins",
                price: 1050,
                discountPrice: 900,
                duration: "4h 30m",
                level: "Advanced",
                coverImage: "images/courses/course-wordpress.jpg",
                teacherName: "Clevaio Simon",
                teacherPhoto: "images/teacher/clevaio_simon.jpg",
                isPopular: true,
                isTrending: false
            }
        ]
    }

    //Khai báo biến mảng hằng số chứa danh sách tên các thuộc tính
    var gCOURSES_COLS = ['stt', 'coverImage', 'courseCode', 'courseName', 'level' , 'duration', 'price', 'discountPrice', 'teacherName', 'teacherPhoto', 'isPopular', 'isTrending', 'action'];
    var gSTT = 1;
    var gCourseObj = gCoursesDB.courses;
    var gCourseId = 0;
    //Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
    const gSTT_COL = 0;
    const gCOVER_IMAGE_COL = 1;
    const gCOURSES_CODE_COL = 2;
    const gCOURSES_NAME_COL = 3;
    const gLEVEL_COL = 4;
    const gDURATION_COL = 5;
    const gPRICE_COL = 6;
    const gDISCOUNT_PRICE_COL = 7;
    const gTEACHER_NAME_COL = 8;
    const gTEACHER_PHOTO_COL = 9;
    const gPOPULAR_COL = 10;
    const gTRENDING_COL = 11;
    const gACTION_COL = 12;
    // Đổ dữ liệu vào bảng và thực hiện nút Sửa, Xoá
    var gCourseTable  = $('#courses-table').DataTable({
        columns: [
            {data: gCOURSES_COLS[gSTT_COL]},
            {data: gCOURSES_COLS[gCOVER_IMAGE_COL]},
            {data: gCOURSES_COLS[gCOURSES_CODE_COL]},
            {data: gCOURSES_COLS[gCOURSES_NAME_COL]},
            {data: gCOURSES_COLS[gLEVEL_COL]},
            {data: gCOURSES_COLS[gDURATION_COL]},
            {data: gCOURSES_COLS[gPRICE_COL]},
            {data: gCOURSES_COLS[gDISCOUNT_PRICE_COL]},
            {data: gCOURSES_COLS[gTEACHER_NAME_COL]},
            {data: gCOURSES_COLS[gTEACHER_PHOTO_COL]},
            {data: gCOURSES_COLS[gPOPULAR_COL]},
            {data: gCOURSES_COLS[gTRENDING_COL]},
            {data: gCOURSES_COLS[gACTION_COL]},
        ],
        columnDefs: [
            {
                targets: gSTT_COL,
                render: function(){
                    return gSTT++;
                }
            },
            {
                targets: gCOVER_IMAGE_COL,
                className: 'text-center',
                data: 'img',
                render: function(data){
                    return '<img src="' + data + '" alt="' + data + '"height="100%" width="100%">';
                }
            },
            {
                targets: gPRICE_COL,
                render: function(data){
                    return ("$" + data);
                }
            },
            {
                targets: gDISCOUNT_PRICE_COL,
                render: function(data){
                    return ("$" + data);
                }
            },
            {
                targets: gTEACHER_PHOTO_COL,
                data: 'img',
                render: function(data){
                    return '<img src="' + data + '" alt="' + data + '"height="75%" width="75%" class="rounded-circle">';
                }
            },
            {
                targets: gPOPULAR_COL,
                render: function(data){
                    return getChecked(data);
                }
            },
            {
                targets: gTRENDING_COL,
                render: function(data){
                    return getChecked(data);
                }
            },
            {   
                targets: gACTION_COL,
                defaultContent: `<button class="btn btn-primary form-control btn-update">Edit</button>
                                <button class='btn btn-danger form-control btn-delete'>Delete</button>`
            },          
        ],
    });
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    // 2 - C: Gán sự kiện create
    $('#btn-add-courses').on('click', function(){
        $('#insert-course-modal').modal();
        clearInsertModalCourseForm();
        //$('#inp-modal-course-code').removeAttr('disabled');
    });
    // Gán sự kiện cho nút tạo mới course
    $('#btn-modal-create').on('click', function(){
        onBtnModalCreateCourseClick();
    });
    // 3- U: Gán sự kiện Update - sửa course
    $(document).on('click', '.btn-update', function(){
        onBtnUpdateClick(this);
    });
    // Gán sự kiện cho nút update trên modal
    $('#btn-modal-update').on('click', function(){
        onBtnModalUpdateCourseClick();
    });
    // 4- D: Gán sự kiện delete
    $(document).on('click', '.btn-delete', function(){
        onBtnDeletaClick(this);
    });
    // Gán sự kiện nút confirm xoá
    $('#btn-confirm-delete').on('click', function(){
        onBtnModalConfirmDeleteClick();
    });
    
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
    // Hàm thực thi khi load trang
    function onPageLoading(){
        "use strict";
        //gọi hàm để load dữ liệu mảng courses vào datatable
        loadDataToCourseTable(gCourseObj);
    }
    // Hàm xử lý sự kiện nút create
    function onBtnModalCreateCourseClick(){
        "use strict";
        //Khái báo đối tượng
        var vInfoCourseObj = {
            id: 0,
            courseCode: "",
            courseName: "",
            coverImage: "",
            discountPrice: 0,
            duration: "",
            isPopular: "",
            isTrending: "",
            level: "",
            price: 0,
            teacherName: "",
            teacherPhoto: "",
        };
        //B1: thu thấp dữ liệu
        getValueInputModalCreate(vInfoCourseObj);
        //B2: validate dữ liệu
        var vCheck = validateDataInputModal(vInfoCourseObj);
        if(vCheck){
            //B3: truyền vào course database
            gCourseObj.push(vInfoCourseObj);
            console.log(gCourseObj);
            alert("Thêm thành công!");
            $('#insert-course-modal').modal('hide');
            clearInsertModalCourseForm();
            loadDataToCourseTable(gCourseObj);
        }
    }
    //Hàm xử lý sự kiện nút update được ấn
    function onBtnUpdateClick(parmaUpdate){
        "use strict";
        //lưu thông tin CourseId đang được edit vào biến toàn cục
        gCourseId = getIdCourseFromButton(parmaUpdate);
        console.log("Course ID được update là: " + gCourseId);
        showCourseDataToModal(gCourseId);
        $('#update-course-modal').modal();
    }
    // Hàm xử lý xự kiện update trên modal
    function onBtnModalUpdateCourseClick(){
        "use strict";
        // Khai báo đối tượng chứa data
        var vInfoCourseObj = {
            id: 0,
            courseCode: "",
            courseName: "",
            coverImage: "",
            discountPrice: 0,
            duration: "",
            isPopular: "",
            isTrending: "",
            level: "",
            price: 0,
            teacherName: "",
            teacherPhoto: "",
        };
        //B1: thu thập dữ liệu để update
        getCourseDataUpdate(vInfoCourseObj);
        //B2: validate dữ liệu 
        var vCheck = validateDataInputModalUpdate(vInfoCourseObj);
        if(vCheck){
            console.log(gCourseObj);
            //update
            updateCourse(vInfoCourseObj)
            //load lại dữ liệu đã update lên bảng
            loadDataToCourseTable(gCourseObj);
            //xoá dữ liệu tren modal update
            clearUpdateModalCourseForm();
            alert("Cập nhật thành công!");
            $('#update-course-modal').modal('hide');
        }
    }
    // Hàm gọi khi nút Delete được ấn
    function onBtnDeletaClick(paramBtn){
        "use strict";
        //lưu thông tin courseId đang được delete cào biến toàn cục
        gCourseId = getIdCourseFromButton(paramBtn);
        console.log("Course ID cần delete là: " + gCourseId);
        $('#delete-confirm-modal').modal();
    }
    // Hàm xử lý sự kiện confirm delete trên modal
    function onBtnModalConfirmDeleteClick(){
        "use strict";
        //B1: thu thập dữ liệu
        //B2: validate
        //B3: delete course
        deleteCourse(gCourseId);
        console.log("CourseId đã xoá là: " + gCourseId);
        alert("Xoá CourseId: " + gCourseId + " thành công!");
        $('#delete-confirm-modal').modal('hide');
        //load lại bảng
        loadDataToCourseTable(gCourseObj);
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm xử lý load dữ liệu course vào table
    function loadDataToCourseTable(paramCourses){
        "use strict";
        gSTT = 1;
        gCourseTable.clear();
        gCourseTable.rows.add(paramCourses);
        gCourseTable.draw();
    }
    // Hàm render lại cột popular và trending thành kiểu checkbox
    function getChecked(paramData){
        "use strict";
        var vCheck = '<input type="checkbox" checked="checked" disabled>';
        if(!paramData){
            vCheck = '<input type="checkbox" disabled>';
        }
    
        return vCheck;
    }
    // Hàm xoá trắng input trên modal insert
    function clearInsertModalCourseForm(){
        "use strict";
        $('#inp-modal-course-code').val("");
        $('#inp-modal-course-name').val("");
        $('#input-modal-duration').val("");    
        $('#input-modal-price').val("");    
        $('#input-modal-discount').val("");   
        $('#inp-modal-teacher-name').val("");
        $('#inp-modal-course-photo').val("");    
        $('#inp-modal-teacher-photo').val("");    
    }
    // Hàm xoá trắng input trên modal update
    function clearUpdateModalCourseForm(){
        "use strict";
        $('#inp-update-modal-course-code').val("");
        $('#inp-update-modal-course-name').val("");
        $('#input-update-modal-duration').val("");    
        $('#input-update-modal-price').val("");    
        $('#input-update-modal-discount').val("");   
        $('#inp-update-modal-teacher-name').val("");
        $('#inp-update-modal-course-photo').val("");    
        $('#inp-update-modal-teacher-photo').val("");
    }
    // Hàm lấy dữ liệu từ modal create
    function getValueInputModalCreate(paramInput){
        "use stricct";
        paramInput.id = getNextId();
        paramInput.courseCode = $('#inp-modal-course-code').val();
        paramInput.courseName = $('#inp-modal-course-name').val();
        paramInput.level = $('#select-modal-level').val();
        var vKiemTraBoleanPopular = false;
        if ( $('#select-modal-popular').val() == "true"){
            vKiemTraBoleanPopular = true;
        }
        paramInput.isPopular = vKiemTraBoleanPopular;
        var vKiemTraBoleanTrending = false;
        if ( $('#select-modal-trending').val() == "true"){
            vKiemTraBoleanTrending = true;
        }
        paramInput.isTrending = vKiemTraBoleanTrending;
        paramInput.duration = $('#input-modal-duration').val(); 
        paramInput.price = $('#input-modal-price').val();       
        paramInput.discountPrice = $('#input-modal-discount').val();
        paramInput.teacherName = $('#inp-modal-teacher-name').val();
        paramInput.coverImage =  $('#inp-modal-course-photo').val();
        paramInput.teacherPhoto = $('#inp-modal-teacher-photo').val();    
    }
    // Hám lssy ra được id course tiếp theo, dùn gkhi thêm mới course
    function getNextId(){
        "use strict";
        var vNexxtId = 0;
        //nếu mảng chứa có phần tử nào, thì id sẽ bắt đầu từ 1
        if (gCourseObj.length == 0){
            vNexxtId = 1;
        }
        else { //id tiếp theo id của phần tử cuối cùng cộng thêm 1
            vNexxtId = gCourseObj[gCourseObj.length - 1].id + 1;
        }
        return vNexxtId;
    }
    // Hàm kiểm tra dữ liệu input modal create
    function validateDataInputModal(paramInput){
        "use strict";
        ///kiểm tra course code
        if(isCourseExistCreate(paramInput.courseCode)){
            alert("Course code is already exist!");
            return false;
        }
        //hàm kiểm tra course name
        if(paramInput.courseName == ""){
            alert("Course name is not fill!")
            return false;
        }
        //course name không được nhập số
        if( isFinite(paramInput.courseName)){
            alert("Course name is not number!")
            return false;
        }
        // hàm kiểm tra cover image
        if(paramInput.coverImage == ""){
            alert("Cover image is not fill!")
            return false;
        }
        if(isFinite(paramInput.coverImage)){
            alert("Cover image is not number!")
            return false;
        }
        // hàm kiểm tra nhập price
        if( !isFinite(paramInput.price)){
            alert("Price is not fill!")
            return false;
        }
        // nếu dícount > price thì báo lỗi
        if( !isFinite(paramInput.discountPrice)){
            alert("Discount Price is not fill!")
            return false;
        }
        if (paramInput.discountPrice >= paramInput.price){
            alert("Discount Price must be smaller than price!");
            return false;
        }
        //hàm kiểm tra giá trị teacher name
        if(paramInput.teacherName == ""){
            alert("Teacher is not fill!");
            return false;
        }
        if(isFinite(paramInput.teacherName)){
            alert("Teacher name is not number!")
            return false;
        }
        // hàm kiểm tra giá trị teacher photo
        if(paramInput.teacherPhoto == ""){
            alert("Teacher PHoto is not fill!");
            return false;
        }
        // Teacher photo (src) không được nhập số
        if(isFinite(paramInput.teacherPhoto)){
            alert("Teacher photo is not number!")
            return false;
        }
        return true;
    }
    // Hám check mã course đã tôn tại chưa
    function isCourseExistCreate(paramCourseCode){
        "use strict";
        var vCourseExist = false;
        var vLoopIndex = 0;
        while ( !vCourseExist && vLoopIndex < gCourseObj.length){
            if(gCourseObj[vLoopIndex].courseCode === paramCourseCode){
                vCourseExist = true;
            }
            else{
                vLoopIndex ++;
            }
        }
        return vCourseExist;
    }
    // Hàm dựa vào button sửa hoặc xoá xác định được course id
    function getIdCourseFromButton(paramBtn){
        "use strict";
        var vTableRow = $(paramBtn).parents('tr');
        var vCourseRowData = gCourseTable.row(vTableRow).data();
        return vCourseRowData.id;
    }
    // Hàm show course obj lên modal
    function showCourseDataToModal(paramCourseId){
        "use strict";
        var vCourseIndex = getCourseIndexFromId(paramCourseId);
        $('#inp-update-modal-course-code').val(gCourseObj[vCourseIndex].courseCode).attr('disabled','disabled'); // không cho sửa mã course
        $('#inp-update-modal-course-name').val(gCourseObj[vCourseIndex].courseName);
        $('#inp-update-modal-course-photo').val(gCourseObj[vCourseIndex].coverImage);
        $('#input-update-modal-duration').val(gCourseObj[vCourseIndex].duration);
        $('#select-update-modal-level').val(gCourseObj[vCourseIndex].level);
        $('#input-update-modal-price').val(gCourseObj[vCourseIndex].price);
        $('#input-update-modal-discount').val(gCourseObj[vCourseIndex].discountPrice);
        $('#inp-update-modal-teacher-name').val(gCourseObj[vCourseIndex].teacherName);
        $('#inp-update-modal-teacher-photo').val(gCourseObj[vCourseIndex].teacherPhoto);
        var vGetVlueIsPopular = gCourseObj[vCourseIndex].isPopular;
        if(vGetVlueIsPopular){
            $('#select-update-modal-popular').val("true");
        }
        else {
            $('#select-update-modal-popular').val("false");
        }
        var vGetVlueIsTrending = gCourseObj[vCourseIndex].isTrending;
        if(vGetVlueIsTrending){
            $('#select-update-modal-trending').val("true");
        }
        else {
            $('#select-update-modal-trending').val("false");
        }
    }
    // Hàm lấy course index from course id
    function getCourseIndexFromId(paramCourseId){
        "use strict";
        var vCourseIndex = -1;
        var vCourseFound = false;
        var vLoopIndex = 0;
        while( !vCourseFound && vLoopIndex < gCourseObj.length){
            if(gCourseObj[vLoopIndex].id === paramCourseId){
                vCourseIndex = vLoopIndex;
                vCourseFound = true;
            }
            else {
                vLoopIndex ++;
            }
        }
        return vCourseIndex;
    }
    // Hàm thu thập dữ liệu để update
    function getCourseDataUpdate(paramInput){
        "use strict";
        paramInput.id = gCourseId;
        paramInput.courseCode = $('#inp-update-modal-course-code').val();
        paramInput.courseName = $('#inp-update-modal-course-name').val();
        paramInput.level = $('#select-update-modal-level').val();
        var vKiemTraBoleanPopular = false;
        if ( $('#select-update-modal-popular').val() == "true"){
            vKiemTraBoleanPopular = true;
        }
        paramInput.isPopular = vKiemTraBoleanPopular;
        var vKiemTraBoleanTrending = false;
        if ( $('#select-update-modal-trending').val() == "true"){
            vKiemTraBoleanTrending = true;
        }
        paramInput.isTrending = vKiemTraBoleanTrending;
        paramInput.duration = $('#input-update-modal-duration').val(); 
        paramInput.price = parseInt($('#input-update-modal-price').val());
        paramInput.discountPrice = parseInt($('#input-update-modal-discount').val());
        paramInput.teacherName = $('#inp-update-modal-teacher-name').val();
        paramInput.coverImage =  $('#inp-update-modal-course-photo').val();
        paramInput.teacherPhoto = $('#inp-update-modal-teacher-photo').val(); 
    }
    // Hàm validate dât để update
    function validateDataInputModalUpdate(paramInput){
        "use strict";    
        //hàm kiểm tra course name
        if(paramInput.courseName == ""){
            alert("Course name is not fill!")
            return false;
        }
        //course name không được nhập số
        if( isFinite(paramInput.courseName)){
            alert("Course name is not number!")
            return false;
        }
        // hàm kiểm tra cover image
        if(paramInput.coverImage == ""){
            alert("Cover image is not fill!")
            return false;
        }
        if(isFinite(paramInput.coverImage)){
            alert("Cover image is not number!")
            return false;
        }
        // hàm kiểm tra nhập price
        if( !isFinite(paramInput.price)){
            alert("Price is not fill!")
            return false;
        }
        // nếu dícount > price thì báo lỗi
        if( !isFinite(paramInput.discountPrice)){
            alert("Discount Price is not fill!")
            return false;
        }
        if (paramInput.discountPrice >= paramInput.price){
            alert("Discount Price must be smaller than price!");
            return false;
        }
        //hàm kiểm tra giá trị teacher name
        if(paramInput.teacherName == ""){
            alert("Teacher is not fill!");
            return false;
        }
        if(isFinite(paramInput.teacherName)){
            alert("Teacher name is not number!")
            return false;
        }
        // hàm kiểm tra giá trị teacher photo
        if(paramInput.teacherPhoto == ""){
            alert("Teacher PHoto is not fill!");
            return false;
        }
        // Teacher photo (src) không được nhập số
        if(isFinite(paramInput.teacherPhoto)){
            alert("Teacher photo is not number!")
            return false;
        }
        return true;
    }
    // Hàm thực hiện update course trong mảng
    function updateCourse(paramCourseObj){
        "use strict";
        //lấy chỉ số index trong mảng
        var vCourseIndex = getCourseIndexFromId(paramCourseObj.id);
        //Cập nhật courseObj vào mảng
        gCourseObj.splice(vCourseIndex, 1, paramCourseObj);
    }
    // Hàm thực hiện delete course trong mảng
    function deleteCourse(paramCourseId){
        "use strict";
        //lấy chỉ số index trong mảng
        var vCourseIndex = getCourseIndexFromId(paramCourseId);
        //Xoá course khỏi mảng
        gCourseObj.splice(vCourseIndex, 1);
    }
})